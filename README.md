Google's Android Kernel Build System
=====================================

Current Device: Cheetah / Panther - Pixel 7 / Pixel 7 Pro

Current Branch: android-gs-pantah-5.10-android13-qpr2

This is based on google branches and source code.

Everything is currently pulled from google save for a few repositories listed here:

>"android/kernel manifest"
>https://gitlab.com/hdpk/kernel_manifest/-/tree/pantah-13

>"anykernel3"
>https://gitlab.com/hdpk/anykernel3/-/tree/pantah-13

>"build/kernel"
>https://gitlab.com/hdpk/android_build/-/tree/pantah-13

>"private/google-modules/wlan/bcmdhd4389"
>https://gitlab.com/hdpk/google-modules-wlan-bcmdhd4389/-/tree/pantah-13

>"private/gs-google/"
>https://gitlab.com/HolyAngel/pantah/-/tree/pantah-13

>"private/gs-google/KernelSU"
>https://gitlab.com/hdpk/KernelSU/-/tree/main

Warning: Occasionally things may be changed or force-pushed without notice.

In Order To Build
====================

>mkdir -p pixel7 $$ cd pixel7

>repo init -u https://gitlab.com/hdpk/kernel_manifest -b pantah-13

>repo sync -j 8

>BUILD_CONFIG=private/gs-google/build.config.cloudripper build/build.sh
